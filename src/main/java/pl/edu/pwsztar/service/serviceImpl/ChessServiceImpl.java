package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame king;
    private RulesOfGame pawn;
    private RulesOfGame queen;
    private RulesOfGame rock;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Pawn") RulesOfGame pawn,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rock") RulesOfGame rock) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.pawn = pawn;
        this.queen = queen;
        this.rock = rock;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        int xStart = getX(figureMoveDto.getStart()), yStart = getY(figureMoveDto.getStart()),
                xEnd = getX(figureMoveDto.getDestination()), yEnd = getY(figureMoveDto.getDestination());
        switch(figureMoveDto.getType()) {
            case BISHOP:
                return bishop.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case KNIGHT:
                return knight.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case KING:
                return king.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case PAWN:
                return pawn.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case QUEEN:
                return queen.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case ROCK:
                return rock.isCorrectMove(xStart, yStart, xEnd, yEnd);
        }
        return false;
    }

    private int getX(String str) {
        return (int) str.charAt(0) + 1 - 'a';
    }

    private int getY(String str) {
        return Character.getNumericValue(str.charAt(2));
    }
}
